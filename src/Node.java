/**
 * 
 */

/**
 * @package 
 * @project Work04 
 * @author WangChen
 * @time 2018年11月6日, 下午12:49:48
 * @description
 */
public class Node {
	private int workIndex;//作业的编号
	private int peopleIndex;//人的编号
	private int workTime;//这个人做这个作业花费的时间
	Node(){
		workIndex = 0;
		peopleIndex = 0;
		workTime = 0;
	}
	
	Node(int wi, int p, int wt){
		workIndex = wi;
		peopleIndex = p;
		workTime = wt;
	}
	
	public void setWorkIndex(int w){
		workIndex = w;
	}
	
	public void setPeopleIndex(int p){
		peopleIndex = p;
	}
	
	public void setWorkTime(int w){
		workTime = w;
	}
	
	public int getWorkIndex(){
		return workIndex;
	}
	
	public int getWorkTime(){
		return workTime;
	}
	
	public int getPeopleIndex(){
		return peopleIndex;
	}
}
